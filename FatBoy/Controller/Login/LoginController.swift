//
//  LoginController.swift
//  FatBoy
//
//  Created by Innovadeaus on 2/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import SwiftyJSON

class LoginController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var password: UITextField!
    let alart = SweetAlert()
    var authenticationService: AuthenticationService?
    var apiCommunicatorHelper: APICommunicator?
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginController.dismissKeyboard))
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        initApiCommunicatorHelper()
        self.mobileNumber.delegate = self
        self.password.delegate = self
        self.keyboardCheck()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func initView(){
        loginButton.roundCorners(corners: [.topRight,.topLeft,.bottomRight,.bottomLeft], radius: 10)
        let userid  = Helpers.getIntValueForKey(Constants.USER_ID)
        if userid != -1 {
            navigateToHome()
        }
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        authenticationService = AuthenticationService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    func doLogin(_ password: String, mobileNumber: String) {
          
          let params = [
              "username": mobileNumber,
              "password": password,
          ]
        print(params)
          authenticationService?.doLogin(params)
      }

    
    @IBAction func confirm(_ sender: Any) {
        if  mobileNumber.text!.count > 10 {
            if password.text!.count > 5 {
              doLogin(password.text!, mobileNumber: mobileNumber.text! )
            }else{
              alart.showAlert("", subTitle: "Wrong Password", style: .error)
            }
            
        }else {
            alart.showAlert("", subTitle: "Mobile number will be more than 10 character", style: .error)
          }
     }
    
    
    ///Response
    func getLoginResponse(_ data: [String: Any], statusCode: Int) {
        
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            print(response)
            if !response.isEmpty {
                if let success = response["success"].bool{
                    if success == true{
                        if let userInfo = response.rawString() {
                          Helpers.setStringValueWithKey(userInfo, key: Constants.USER_INFO)
                        }
                        if let data = response["data"].dictionary {
                       
                            if let token = data["token"]?.string {
                                Helpers.setStringValueWithKey(token, key: Constants.ACCESS_TOKEN)
                             }
                            if let member_id = data["auth_id"]?.int {
                                Helpers.setIntValueWithKey(member_id, key: Constants.MEMBER_ID)
                              
                            }
                            
                            if let user_id = data["id"]?.int {
                                Helpers.setIntValueWithKey(user_id, key: Constants.USER_ID)
                                navigateToHome()
                                
                                 
                            }
                            if let photoUrl = data["photo_url"]?.string{
                                Helpers.setStringValueWithKey(photoUrl, key: Constants.USER_IMAGE)
                            }
                            if let firstName = data["first_name"]?.string{
                                if let lastName = data["last_name"]?.string{
                                    let name = firstName + " " + lastName
                                    Helpers.setStringValueWithKey(name, key: Constants.FULLNAME)
                                }else{
                                    Helpers.setStringValueWithKey(firstName, key: Constants.FULLNAME)
                                }
                            }
                        }
                    }else{
                        if let msg = response["message"].string{
                            alart.showAlert("Faild", subTitle: msg, style: .error)
                        }
                       
                    }
                }
                
             }
            
        }else{
           let response = JSON(data)
            if let msg = response["message"].string{
               alart.showAlert("Faild", subTitle: msg, style: .error)
            }
        }
        
      }
    
    
}

extension LoginController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
                  getLoginResponse(data, statusCode: statusCode)
              }
    }
}
extension LoginController {
    func hideKeyboardWhenTappedAround() {
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    func keyboardCheck() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= 0
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            
        }
    }
}

