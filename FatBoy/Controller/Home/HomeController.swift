//
//  HomeController.swift
//  FatBoy
//
//  Created by Innovadeaus on 30/1/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftQRScanner
var isComeFromHome = false
import CollectionViewSlantedLayout
class HomeController: BaseController {
    @IBOutlet weak var exerciseGide: UICollectionView!
    @IBOutlet weak var blogCollection: UICollectionView!
    @IBOutlet weak var checkinButton: UIButton!
    @IBOutlet weak var checkinViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var collectionViewLayout: CollectionViewSlantedLayout!
    
    @IBOutlet weak var checkInView: UIView!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var blogView: UIView!
    @IBOutlet weak var bmiView: UIView!
    @IBOutlet weak var exerciseView: UIView!
    
    var globalservice: GlobalService?
    var userService : UserService?
    var apiCommunicatorHelper: APICommunicator?
    let alart = SweetAlert()
    var settingService : SettingService?
    
    var blogpost : [JSON] = []
    var blogCategorisPost : [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        initApiCommunicatorHelper()
        getBlogData()
        getSettingData()
        // Do any additional setup after loading the view.
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        globalservice = GlobalService(self.view, communicator: apiCommunicatorHelper!)
        userService = UserService(self.view, communicator: apiCommunicatorHelper!)
        settingService = SettingService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
  
    func initView(){
     
        collectionViewLayout.invalidateLayout()
        checkInView.roundCornersForView(corners: [.topLeft,.topRight], radius: 10)
        weightView.roundCornersForView(corners: [.topLeft,.topRight], radius: 10)
        blogView.roundCornersForView(corners: [.topRight,.topLeft], radius: 10)
         bmiView.roundCornersForView(corners: [.topRight,.topLeft], radius: 10)
        exerciseView.roundCornersForView(corners: [.topRight,.topLeft], radius: 10)
        navigationController?.isNavigationBarHidden = true
        collectionViewLayout.isFirstCellExcluded = true
        collectionViewLayout.isLastCellExcluded = true
        collectionViewLayout.slantingDirection = .downward
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.zIndexOrder = .ascending
        collectionViewLayout.slantingSize = 10
        collectionViewLayout.lineSpacing = 0
              let rect = CGRect(x: 0, y: 0, width: 0, height: 0)
        self.collectionViewLayout.collectionView?.scrollRectToVisible(rect, animated: true)
    }

    func navigateToBlog(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "blogC") as! BlogController
        self.navigationController?.show(nextViewController, sender:true)
    }
    func navigateToBlogDetails(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "blogd") as! BlogDetailsController
        self.navigationController?.show(nextViewController, sender:true)
    }
    func navigateToExcerDetails(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "exerD") as! ExerciseDetailsController
        self.navigationController?.show(nextViewController, sender:true)
    }
    
    @IBAction func openMenuBar(_ sender: Any) {
        showAndHide()
    }
    @IBAction func viewAll(_ sender: Any) {
        isComeFromHome = true
        navigateToBlog()
    }
    
    func activeeQrScaner(){
        let scanner = QRCodeScannerController()
        scanner.delegate = self
        self.present(scanner, animated: true, completion: nil)
    }
    @IBAction func checkInOUt(_ sender: Any) {

        if checkinButton.titleLabel?.text == "Check In" {
            activeeQrScaner()
        }else{
            CheckOut()
        }
        
    }

    func getBlogData(){
        globalservice?.getBlogs()
    }
    func getSettingData(){
        settingService?.getSetting()
    }
    func CheckIn(){
        let param = [
            "type" : "in"
        ]
        
        userService!.Attendance(_param: param)
    }
    func CheckOut(){
        let param = [
            "type" : "out"
        ]
        
        userService!.Attendance(_param: param)
    }
    func getAttendanceResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.STATUS_CODE_SUCCESS {
               let response = JSON(data)
               print(response)
               if !response.isEmpty {
                if let success = response["success"].bool{
                    if success == true{
                        if let msg = response["message"].string{
                         alart.showAlert("Attendance", subTitle: msg, style: .success)
                        }
                    }else{
                        if let msg = response["message"].string{
                            alart.showAlert("Attendance", subTitle: msg, style: .error)
                        }
                     }
              
                }
                
            }
        }
    }
    
    func getBlogResponse(_ data: [String: Any], statusCode: Int) {
          
          if statusCode == Constants.STATUS_CODE_SUCCESS {
              let response = JSON(data)
              print(response)
              if !response.isEmpty {
                  if let success = response["success"].bool{
                      if success == true{
                        if let data = response["data"].dictionary{
                            if let blogData = data["data"]?.array{
                                self.blogpost = blogData
                                blogCollection.reloadData()
                            }
                        }
                      }else{
                          if let msg = response["message"].string{
                              alart.showAlert("Faild", subTitle: msg, style: .error)
                          }
                         
                      }
                  }
                  
               }
              
          }else{
             let response = JSON(data)
              if let msg = response["message"].string{
                 alart.showAlert("Faild", subTitle: msg, style: .error)
              }
          }
          
        }
    func getSettingResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.STATUS_CODE_SUCCESS {
               let response = JSON(data)
               print(response)
               if !response.isEmpty {
                if let success = response["success"].bool{
                    if success == true{
                        if let data = response["data"].dictionary{
                            if let updateType = data["update_type"]?.string{
                                if updateType == "SOFT" {
                                    
                                }else{
                                    
                                }
                            }
                            if let showExerciseGuide = data["show_exercise_guide"]?.string{
                                if showExerciseGuide == "0"{
                                    
                                }else{
                                    
                                }
                             }
                            if let attendance = data["attendance"]?.dictionary {
                                if let checkOut = attendance["check_out"]?.string{
                                    if checkOut != ""{
                                        checkInView.isHidden = true
                                        checkinViewHeight.constant = 0
                                    }else{
                                        checkInView.isHidden = false
                                        checkinViewHeight.constant = 60
                                    }
                                }
                                self.checkinButton.setTitle("Check Out", for: .normal)
                            }else{
                                self.checkinButton.setTitle("Check In", for: .normal)
                               }
                            }
                    }else{
                        
                    }
                }
            }
        }
    }
    
    
    
}

extension  HomeController : UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count: Int?
        if collectionView == self.exerciseGide {
            count = 10
        }
        if collectionView == self.blogCollection {
            count = blogpost.count
        }
        return count!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.exerciseGide {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as?  ExerciseCell
            
            return cell!
        }else if collectionView == self.blogCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? BlogCell
            if let title = blogpost[indexPath.row]["title"].string{
                cell?.Title.text = title
            }
       

            if let layout = collectionView.collectionViewLayout as? CollectionViewSlantedLayout {
                cell!.contentView.transform = CGAffineTransform(rotationAngle: layout.slantingAngle)
            }
            
            return cell!
        }else {
            
        }
        return UICollectionViewCell()
    }
    
    
}
extension HomeController: CollectionViewDelegateSlantedLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.exerciseGide {
            if let catArray = blogpost[indexPath.row]["posts"].array{
                self.blogCategorisPost = catArray
               
            }
          navigateToExcerDetails()
        }else{
          navigateToBlogDetails()
        }
        NSLog("Did select item at indexPath: [\(indexPath.section)][\(indexPath.row)]")
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: CollectionViewSlantedLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGFloat {
        return collectionViewLayout.scrollDirection == .vertical ? 318 : 334
    }
}

extension HomeController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let collectionView = blogCollection else {return}
        guard let visibleCells = collectionView.visibleCells as? [BlogCell] else {return}
        for parallaxCell in visibleCells {
            let yOffset = (collectionView.contentOffset.y - parallaxCell.frame.origin.y) / parallaxCell.imageHeight
            let xOffset = (collectionView.contentOffset.x - parallaxCell.frame.origin.x) / parallaxCell.imageWidth
            parallaxCell.offset(CGPoint(x: xOffset * xOffsetSpeed, y: yOffset * yOffsetSpeed))
        }
    }
}
extension HomeController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.SECOND {
                  getBlogResponse(data, statusCode: statusCode)
        }else if methodTag == MethodTags.FIRST{
            getAttendanceResponse(data, statusCode: statusCode)
        }else if methodTag == MethodTags.THIRD{
            getSettingResponse(data, statusCode: statusCode)
        }
    }
}

extension HomeController : QRScannerCodeDelegate {
    func qrScanner(_ controller: UIViewController, scanDidComplete result: String) {
        print("result:\(result)")
         alart.showAlert("success", subTitle: result, style: .success)
        CheckIn()

    }

    func qrScannerDidFail(_ controller: UIViewController, error: String) {
        print("error:\(error)")
    }

    func qrScannerDidCancel(_ controller: UIViewController) {
        print("SwiftQRScanner did cancel")
    }
    
    
}
