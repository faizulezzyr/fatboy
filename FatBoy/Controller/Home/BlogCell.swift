//
//  BlogCell.swift
//  FatBoy
//
//  Created by Innovadeaus on 30/1/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class BlogCell: UICollectionViewCell {
    @IBOutlet weak var blogImage: UIImageView!
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var readTime: UILabel!
    private var gradient = CAGradientLayer()
    override func awakeFromNib() {
        super.awakeFromNib()

        if let backgroundView = backgroundView {
            gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
            gradient.locations = [0.0, 1.0]
            gradient.frame = backgroundView.bounds
            backgroundView.layer.addSublayer(gradient)
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        if let backgroundView = backgroundView {
            gradient.frame = backgroundView.bounds
        }
    }
    var image: UIImage = UIImage() {
           didSet {
               blogImage.image = image
           }
       }

       var imageHeight: CGFloat {
           return (blogImage?.image?.size.height) ?? 0.0
       }

       var imageWidth: CGFloat {
           return (blogImage?.image?.size.width) ?? 0.0
       }

       func offset(_ offset: CGPoint) {
           blogImage.frame = blogImage.bounds.offsetBy(dx: offset.x, dy: offset.y)
       }
    
}
