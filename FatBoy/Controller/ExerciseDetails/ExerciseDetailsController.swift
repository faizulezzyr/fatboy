//
//  ExerciseDetailsController.swift
//  FatBoy
//
//  Created by Innovadeaus on 10/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class ExerciseDetailsController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    @IBAction func back(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    }
    

}
