//
//  AttendanceCell.swift
//  FatBoy
//
//  Created by Innovadeaus on 10/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class AttendanceCell: UITableViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var inTime: UILabel!
    @IBOutlet weak var OutTime: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var colorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
