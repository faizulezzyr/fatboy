//
//  AttendanceController.swift
//  FatBoy
//
//  Created by Innovadeaus on 10/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import AnimatableReload
import SwiftyJSON

class AttendanceController: UIViewController {
    @IBOutlet weak var tableview: UITableView!
    var userService : UserService?
    var apiCommunicatorHelper: APICommunicator?
    let alart = SweetAlert()
    var attendanceList : [JSON] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        initApiCommunicatorHelper()
        GetAttendance()
        
        // Do any additional setup after loading the view.
        
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        userService = UserService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
     func ConvertToDate(_ dateString: String) -> Date {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let date = formatter.date(from: dateString)!
        return date
    }

    @IBAction func back(_ sender: Any) {
           navigateToHome()
       }
    func GetAttendance(){
        userService?.getAttendance()
    }
    func generateRandomColor() -> UIColor {
      let hue : CGFloat = CGFloat(arc4random() % 256) / 256 // use 256 to get full range from 0.0 to 1.0
      let saturation : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from white
      let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from black

      return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }
    func getAttendanceResponse(_ data: [String: Any], statusCode: Int) {
              
              if statusCode == Constants.STATUS_CODE_SUCCESS {
                  let response = JSON(data)
                  print(response)
                  if !response.isEmpty {
                      if let success = response["success"].bool{
                          if success == true{
                            if let data = response["data"].dictionary{
                                if let attendanceData = data["data"]?.array{
                                    self.attendanceList = attendanceData
                                    AnimatableReload.reload(tableView: tableview, animationDirection: "right")
                                }
                            
                            }
                          }else{
                              if let msg = response["message"].string{
                                  alart.showAlert("Faild", subTitle: msg, style: .error)
                              }
                             
                          }
                      }
                      
                   }
                  
              }else{
                 let response = JSON(data)
                  if let msg = response["message"].string{
                     alart.showAlert("Faild", subTitle: msg, style: .error)
                  }
              }
              
            }
    
}
extension AttendanceController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attendanceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? AttendanceCell
        if let dateD = attendanceList[indexPath.row]["date"].string{
            let convertData = Commons.changeDateFormat(dateD, curformat: "yyyy-MM-dd", desiredFormat: "MMM dd,yyyy")
            cell?.date.text = convertData
        }
        if let inTime = attendanceList[indexPath.row]["check_in"].string{
            let convertedInTime = Commons.changeTimeFormat(inTime, curformat: "HH:mm:ss", desiredFormat: "h:mm a")
            cell?.inTime.text = convertedInTime
            if let outTime = attendanceList[indexPath.row]["check_out"].string{
                let convertedOutTime = Commons.changeTimeFormat(outTime, curformat: "HH:mm:ss", desiredFormat: "h:mm a")
                cell?.OutTime.text = convertedOutTime
//                let inTimeDateFormate = ConvertToDate(inTime)
//                let outtimeDateFormate = ConvertToDate(outTime)
//                let calculateTime = Commons.calculateTime(currentDate: outtimeDateFormate, createdDate: inTimeDateFormate)
               // cell?.totalTime.text = calculateTime
            }
        }
        let myColor = generateRandomColor()
        cell?.colorView.backgroundColor = myColor
        cell?.backView.setCardViewForInvoice()
        
        return cell!
    }
    
    
}
extension AttendanceController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
                  getAttendanceResponse(data, statusCode: statusCode)
              }
    }
}
