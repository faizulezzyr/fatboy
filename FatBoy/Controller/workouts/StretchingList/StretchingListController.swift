//
//  StretchingListController.swift
//  FatBoy
//
//  Created by Innovadeaus on 11/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import AnimatableReload

class StretchingListController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        AnimatableReload.reload(tableView: tableView, animationDirection: "right")
    }
    @IBAction func back(_ sender: Any) {
          navigationController?.popViewController(animated: true)
      }
    func navigateToStretchingDetails(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "stretchingDetails") as! stretchingDetailsController
        self.navigationController?.show(nextViewController, sender:true)
    }

}

extension StretchingListController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? StretchingCell
        
        cell?.backView.roundCornersForView(corners: [.topRight,.bottomRight], radius: 27)
         cell?.backViewfirst.roundCornersForView(corners: [.topLeft,.bottomLeft], radius: 10)
        //cell?.backView.setCardView()
        cell?.dayView.roundCornersForView(corners: [.topRight,.topLeft,.bottomRight,.bottomLeft], radius: 25)
        cell?.stretchingimage.roundCornersForView(corners: [.topRight,.topLeft,.bottomRight,.bottomLeft], radius: 27)
        
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigateToStretchingDetails()
    }
    
    

}
