//
//  StretchingCell.swift
//  FatBoy
//
//  Created by Innovadeaus on 11/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class StretchingCell: UITableViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var stretchingimage: UIImageView!
    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var backViewfirst: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

