//
//  workoutController.swift
//  FatBoy
//
//  Created by Innovadeaus on 2/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//


import UIKit
import CollectionViewSlantedLayout

class workoutController: UIViewController {
   
    @IBOutlet weak var collectionViewLayout: CollectionViewSlantedLayout!
    
    @IBOutlet weak var collectionView: UICollectionView!
    

    

    internal var covers = [[String: String]]()

    let reuseIdentifier = "customViewCell"

    override func loadView() {
        super.loadView()
        if let url = Bundle.main.url(forResource: "covers", withExtension: "plist"),
            let contents = NSArray(contentsOf: url) as? [[String: String]] {
            covers = contents
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        collectionViewLayout.isFirstCellExcluded = true
        collectionViewLayout.isLastCellExcluded = true
        collectionViewLayout.slantingDirection = .upward
        collectionViewLayout.scrollDirection = .vertical
        collectionViewLayout.zIndexOrder = .ascending
        collectionViewLayout.slantingSize = 30
        collectionViewLayout.lineSpacing = 0
              let rect = CGRect(x: 0, y: 0, width: 0, height: 0)
        self.collectionViewLayout.collectionView?.scrollRectToVisible(rect, animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        collectionView.reloadData()
//        collectionView.collectionViewLayout.invalidateLayout()
    }

//    override var prefersStatusBarHidden: Bool {
//        return true
//    }

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }
    func navigateToworkoutList(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "workoutList") as! StretchingListController
        self.navigationController?.show(nextViewController, sender:true)
    }


}

extension workoutController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
                            as? CustomCollectionCell else {
            fatalError()
        }

      //  cell.image = UIImage(named: covers[indexPath.row]["picture"]!)!

        if let layout = collectionView.collectionViewLayout as? CollectionViewSlantedLayout {
            cell.contentView.transform = CGAffineTransform(rotationAngle: layout.slantingAngle)
        }

        return cell
    }
}

extension workoutController: CollectionViewDelegateSlantedLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       navigateToworkoutList()
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: CollectionViewSlantedLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGFloat {
        return collectionViewLayout.scrollDirection == .vertical ? 275 : 325
    }
}

extension workoutController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let collectionView = collectionView else {return}
        guard let visibleCells = collectionView.visibleCells as? [CustomCollectionCell] else {return}
        for parallaxCell in visibleCells {
            let yOffset = (collectionView.contentOffset.y - parallaxCell.frame.origin.y) / parallaxCell.imageHeight
            let xOffset = (collectionView.contentOffset.x - parallaxCell.frame.origin.x) / parallaxCell.imageWidth
            parallaxCell.offset(CGPoint(x: xOffset * xOffsetSpeed, y: yOffset * yOffsetSpeed))
        }
    }
}


