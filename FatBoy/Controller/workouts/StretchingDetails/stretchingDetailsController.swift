//
//  stretchingDetailsController.swift
//  FatBoy
//
//  Created by Innovadeaus on 11/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import AnimatableReload

class stretchingDetailsController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       AnimatableReload.reload(collectionView: collectionView, animationDirection: "down")
    }
    @IBAction func back(_ sender: Any) {
          navigationController?.popViewController(animated: true)
      }
    

}
extension stretchingDetailsController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? StretchingDetailsCell
        //cell?.setCardView()
        cell?.backImage.roundCornersForView(corners: [.topRight,.topLeft,.bottomLeft,.bottomRight], radius: 10)
        cell?.shadowView.roundCornersForView(corners: [.bottomLeft,.bottomRight], radius: 10)
        
        return cell!
    }
    
    
}
