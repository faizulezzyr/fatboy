//
//  StretchingDetailsCell.swift
//  FatBoy
//
//  Created by Innovadeaus on 11/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit


class StretchingDetailsCell: UICollectionViewCell {
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var set: UILabel!
    @IBOutlet weak var reps: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    

}
