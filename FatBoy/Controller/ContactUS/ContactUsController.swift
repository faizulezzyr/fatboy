//
//  ContactUsController.swift
//  FatBoy
//
//  Created by Innovadeaus on 12/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactUsController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var apiCommunicatorHelper: APICommunicator?
    var settingService : SettingService?
    var contactList :[JSON] = []
    let alart = SweetAlert()
    override func viewDidLoad() {
        super.viewDidLoad()
        initApiCommunicatorHelper()
        getContactData()

        // Do any additional setup after loading the view.
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        settingService = SettingService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    func getContactData(){
        settingService?.getContactList()
    }
    

    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func back(_ sender: Any) {
        navigateToHome()
    }
    func getContactDataResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.STATUS_CODE_SUCCESS {
               let response = JSON(data)
               print(response)
               if !response.isEmpty {
                if let success = response["success"].bool{
                    if success == true{
                        if let data = response["data"].array{
                          contactList = data
                            tableView.reloadData()
                        }
                    }else{
                        if let msg = response["message"].string{
                            alart.showAlert("Error", subTitle: msg, style: .error)
                        }
                     }
              
                }
                
            }
        }
    }

}
extension ContactUsController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ContactCell
        if let contactNumber = contactList[indexPath.row]["contact_no"].string{
            cell?.phoneNumber.text = contactNumber
        }
        if let branchName = contactList[indexPath.row]["name"].string{
            cell?.name.text = branchName
        }
        if let address = contactList[indexPath.row]["address"].string{
            cell?.address.text = address
        }
        if let email = contactList[indexPath.row]["email"].string{
            cell?.email.text = email
        }
        
        
        return cell!
    }
    
    
}
extension ContactUsController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
                  getContactDataResponse(data, statusCode: statusCode)
        }
    }
}
