//
//  NotificationCell.swift
//  FatBoy
//
//  Created by Innovadeaus on 5/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class NotificationCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var notificationtag: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var notiImage: UIImageView!
    
}
