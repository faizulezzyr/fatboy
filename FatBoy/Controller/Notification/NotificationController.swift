//
//  NotificationController.swift
//  FatBoy
//
//  Created by Innovadeaus on 5/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import SwiftyJSON


class NotificationController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var globalservice: GlobalService?
    var apiCommunicatorHelper: APICommunicator?
    let alart = SweetAlert()
    var notificationList : [JSON] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        initApiCommunicatorHelper()
        getNotificationData()
   
        let layout = VegaScrollFlowLayout()
        collectionView.collectionViewLayout = layout
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: collectionView.frame.width, height: 157)
        layout.sectionInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        globalservice = GlobalService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    
    func getNotificationData(){
        globalservice?.getNotification()
    }
    func getNotificationResponse(_ data: [String: Any], statusCode: Int) {
              
              if statusCode == Constants.STATUS_CODE_SUCCESS {
                  let response = JSON(data)
                  print(response)
                  if !response.isEmpty {
                      if let success = response["success"].bool{
                          if success == true{
                            if let data = response["data"].dictionary{
         
                                if let notificationData = data["data"]?.array{
                                    self.notificationList = notificationData
                                    self.collectionView.reloadData()
                                }
                            
                            }
                          }else{
                              if let msg = response["message"].string{
                                  alart.showAlert("Faild", subTitle: msg, style: .error)
                              }
                             
                          }
                      }
                      
                   }
                  
              }else{
                 let response = JSON(data)
                  if let msg = response["message"].string{
                     alart.showAlert("Faild", subTitle: msg, style: .error)
                  }
              }
              
            }


}
extension NotificationController : UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return notificationList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as?  NotificationCell
        
        if let title = notificationList[indexPath.row]["title"].string{
            cell?.name.text = title
        }
        if let content = notificationList[indexPath.row]["contents"].string{
            let AttributConten = content.convertHTML()
            cell?.text.attributedText = AttributConten
            cell?.text.font = UIFont.systemFont(ofSize: 13)
            cell?.text.textColor = UIColor.white
        }
        if let timeAgo = notificationList[indexPath.row]["published_at"].string{
            cell?.time.text = timeAgo
        }
        cell?.backView.setCardViewForNotification()
        cell?.notificationtag.layer.cornerRadius = 3
        cell?.notificationtag.layer.masksToBounds = true
        
        return cell!
    }
    
    
}
extension NotificationController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getNotificationResponse(data, statusCode: statusCode)
        }
    }
}
