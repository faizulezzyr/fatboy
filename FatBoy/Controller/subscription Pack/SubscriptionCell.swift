//
//  SubscriptionCell.swift
//  FatBoy
//
//  Created by Innovadeaus on 4/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class SubscriptionCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var subButton: UIButton!
    @IBOutlet weak var widthConstant: NSLayoutConstraint!
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var stack1: UIStackView!
    @IBOutlet weak var stack2: UIStackView!
    @IBOutlet weak var stack3: UIStackView!
    @IBOutlet weak var stack4: UIStackView!
    @IBOutlet weak var stack5: UIStackView!
    @IBOutlet weak var stack6: UIStackView!
    @IBOutlet weak var text1: UILabel!
    @IBOutlet weak var text2: UILabel!
    @IBOutlet weak var text3: UILabel!
    @IBOutlet weak var text4: UILabel!
    @IBOutlet weak var text5: UILabel!
    @IBOutlet weak var text6: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var name: UILabel!
    
    
    
}
