//
//  SubcriptionController.swift
//  FatBoy
//
//  Created by Innovadeaus on 4/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import CenteredCollectionView
import AnimatableReload
import SwiftyJSON
import SSLCommerz



class SubcriptionController: UIViewController,SSLCommerzDelegate {

    
   
    var imageArry  = ["sub5","sub6","sub5","sub6"]
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var centeredCollectionViewFlowLayout: CenteredCollectionViewFlowLayout!
    
    let cellPercentWidth: CGFloat = 0.7
    var userService : UserService?
    var apiCommunicatorHelper: APICommunicator?
    let alart = SweetAlert()
    var pricePlan : [JSON] = []
    var sslCom: SSLCommerz?

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        initApiCommunicatorHelper()
        getPricePlan()

        
                // Get the reference to the `CenteredCollectionViewFlowLayout` (REQUIRED STEP)
    }
    func initView(){
        // Modify the collectionView's decelerationRate (REQUIRED STEP)
        collectionView.decelerationRate = UIScrollView.DecelerationRate.fast

        // Assign delegate and data source
        collectionView.delegate = self
        collectionView.dataSource = self

        // Configure the required item size (REQUIRED STEP)
        centeredCollectionViewFlowLayout.itemSize = CGSize(
            width: view.bounds.width * cellPercentWidth,
            height: view.bounds.height * cellPercentWidth * cellPercentWidth
        )

        // Configure the optional inter item spacing (OPTIONAL STEP)
        centeredCollectionViewFlowLayout.minimumLineSpacing = 20

        // Get rid of scrolling indicators
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
    
    }
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        userService = UserService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    func transactionCompleted(withTransactionData transactionData: TransactionDetails?) {
        
    }
    
    @IBAction func back(_ sender: Any) {
        navigateToHome()
    }
    @objc func completePay() {
        sslCom = SSLCommerz.init(integrationInformation: .init(storeID: "innov5e4a3590ce638", storePassword: "innov5e4a3590ce638@ssl", totalAmount: 3000.50, currency: "BDT", transactionId: "64746", productCategory: "None"), emiInformation: nil, customerInformation: .init(customerName: "Demo customer", customerEmail: "faizul.ezzyr@gmail.com", customerAddressOne: "No address", customerCity: "dhaka", customerPostCode: "nocode", customerCountry: "BD", customerPhone: "01954591609"), shipmentInformation: nil, productInformation: nil, additionalInformation: nil)
        sslCom?.delegate = self
        sslCom?.start(in: self, shouldRunInTestMode: true)
    }
    func getPricePlan() {
        userService?.getPricePlan()
    }
    func getPricePlanResponse(_ data: [String: Any], statusCode: Int) {
          if statusCode == Constants.STATUS_CODE_SUCCESS {
                 let response = JSON(data)
                 print(response)
                 if !response.isEmpty {
                  if let success = response["success"].bool{
       
                    if success == true{
                        if let data = response["data"].dictionary{
                            if let priceData = data["data"]?.array{
                                self.pricePlan = priceData
                                    AnimatableReload.reload(collectionView: collectionView, animationDirection: "left")
                            }
                        }
                     }
                
                  }else{
                    
                 }
                  
              }
          }
      }
    
}
extension SubcriptionController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pricePlan.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? SubscriptionCell
        
        if let name = pricePlan[indexPath.row]["title"].string{
            cell?.name.text = name
            
        }
        if let services = pricePlan[indexPath.row]["services"].array {
            if services.count != 0 {
                for index in 0..<services.count {
                    if index == 0 {
                        if let text1Value = services[index]["name"].string{
                            cell?.text1.text = text1Value
                            cell?.stack1.isHidden = false
                        }
                    }else if index == 1{
                        if let text1Value = services[index]["name"].string{
                            cell?.text2.text = text1Value
                            cell?.stack2.isHidden = false
                        }
                    }else if index == 2{
                        if let text1Value = services[index]["name"].string{
                            cell?.text3.text = text1Value
                            cell?.stack3.isHidden = false
                        }
                    }else if index == 3{
                        if let text1Value = services[index]["name"].string{
                            cell?.text4.text = text1Value
                            cell?.stack4.isHidden = false
                        }
                    }else if index == 4{
                        if let text1Value = services[index]["name"].string{
                            cell?.text5.text = text1Value
                            cell?.stack5.isHidden = false
                        }
                    }else {
                        if let text1Value = services[index]["name"].string{
                            cell?.text6.text = text1Value
                            cell?.stack6.isHidden = false
                        }
                    }
                }
            }
        }
        cell?.backView.setCardView()
        cell?.widthConstant.constant = self.view.frame.width - 120
        cell?.subButton.layer.cornerRadius = 15
        cell?.subButton.layer.masksToBounds = true
        cell?.backImage.image = UIImage(named: imageArry[indexPath.row])
        cell?.subButton.tag = indexPath.row
        cell?.subButton.addTarget(self, action: #selector(completePay), for: UIControl.Event.touchUpInside)
        


        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    

    }
}
extension SubcriptionController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
                  getPricePlanResponse(data, statusCode: statusCode)
              }
    }
}
