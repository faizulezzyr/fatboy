//
//  fullBlogCell.swift
//  FatBoy
//
//  Created by Innovadeaus on 6/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class fullBlogCell: UICollectionViewCell {
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var author: UILabel!
    
}
