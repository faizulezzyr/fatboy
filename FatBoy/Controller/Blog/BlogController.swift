//
//  BlogController.swift
//  FatBoy
//
//  Created by Innovadeaus on 6/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import AnimatableReload
import SwiftyJSON
import Kingfisher
var blogCategorisPost : [JSON] = []
var blogIndex = 0
class BlogController: UIViewController,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var blogCollectionView: UICollectionView!
    @IBOutlet weak var categorisCollection: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    var currentCatIndex = 0
    var globalservice: GlobalService?
    var apiCommunicatorHelper: APICommunicator?
    let alart = SweetAlert()
    var blogpost : [JSON] = []
 
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initApiCommunicatorHelper()
        getBlogPost()
        self.categorisCollection.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        globalservice = GlobalService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    func initView(){
        screenSize = UIScreen.main.bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: screenWidth/2 - 15 , height: (screenHeight/3) - 60)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 10
        blogCollectionView!.collectionViewLayout = layout
        
        let layoutCat: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
           layoutCat.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
           layoutCat.itemSize = CGSize(width: screenWidth/3 - 10, height: 40)
           layoutCat.minimumInteritemSpacing = 0
           layoutCat.scrollDirection = .horizontal
           layoutCat.minimumLineSpacing = 0
           categorisCollection!.collectionViewLayout = layoutCat
        

        if isComeFromHome == true{
            backButton.isHidden = true
            isComeFromHome = false
        }else{
            backButton.isHidden = false
        }
        
    }

    
    @IBAction func back(_ sender: Any) {
        navigateToHome()
    }
    
    func navigateToBlogDetails(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "blogd") as! BlogDetailsController
        self.navigationController?.show(nextViewController, sender:true)
    }
    func getBlogPost(){
        globalservice?.getBlogs()
    }
    func getBlogResponse(_ data: [String: Any], statusCode: Int) {
          
          if statusCode == Constants.STATUS_CODE_SUCCESS {
              let response = JSON(data)
              print(response)
              if !response.isEmpty {
                  if let success = response["success"].bool{
                      if success == true{
                        if let data = response["data"].dictionary{
                            if let blogData = data["data"]?.array{
                                self.blogpost = blogData
                                AnimatableReload.reload(collectionView: self.categorisCollection, animationDirection: "right")
                               
                                if let catPost = blogpost[0]["posts"].array {
                                    blogCategorisPost = catPost
                                }
                            AnimatableReload.reload(collectionView: self.blogCollectionView, animationDirection: "up")
                            }
                        }
                      }else{
                          if let msg = response["message"].string{
                              alart.showAlert("Faild", subTitle: msg, style: .error)
                          }
                         
                      }
                  }
                  
               }
              
          }else{
             let response = JSON(data)
              if let msg = response["message"].string{
                 alart.showAlert("Faild", subTitle: msg, style: .error)
              }
          }
          
        }


}
extension BlogController : UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count: Int?
        if collectionView == self.categorisCollection {
            count = blogpost.count
        }
        if collectionView == self.blogCollectionView {
            count = blogCategorisPost.count
        }
        
        return count!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.categorisCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as?  blogChategorisCell
            if let titleCat = blogpost[indexPath.row]["title"].string{
                cell?.title.text = titleCat
            }
    
            if indexPath.row == currentCatIndex {
                cell?.contentView.backgroundColor = UIColor.systemOrange
                cell?.title.textColor = UIColor.white
            }else{
                cell?.contentView.backgroundColor = UIColor.white
                cell?.contentView.layer.borderColor = UIColor.systemOrange.cgColor
                cell?.contentView.layer.borderWidth = 1
                cell?.title.textColor = UIColor.black
            }
            return cell!
        }else if collectionView == self.blogCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? fullBlogCell
            
            if let blogTitle = blogCategorisPost[indexPath.row]["title"].string{
                cell?.title.text = blogTitle
            }
            if let authorName = blogCategorisPost[indexPath.row]["author"].dictionary{
                if let firstName = authorName["first_name"]?.string{
                     cell?.author.text = "Written by \(firstName)"
                }
               
            }
            if let url = blogCategorisPost[indexPath.row]["feature_photo"].string {
                let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let profilePicURL = URL(string: urlString!)
                cell?.coverImage.kf.indicatorType = .activity
                //cell?.coverImage.kf.setImage(with: profilePicURL, placeholder: "placeholder")
                cell?.coverImage.kf.setImage(with: profilePicURL)
            }
            if let readT = blogCategorisPost[indexPath.row][""].string{
                cell?.time.text = readT
            }else{
                cell?.time.text = "5 min read"
            }

            return cell!
        }else {
            
        }
        
        return UICollectionViewCell()
    }

//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if collectionView == self.categorisCollection {
//            let numberOfColumns: CGFloat = 4
//             let itemWidth = (self.categorisCollection!.frame.width  / numberOfColumns)
//             return CGSize(width:itemWidth, height:40)
//        }else if collectionView == self.blogCollectionView {
//                let itemWidht = self.blogCollectionView.frame.width / 2
//                let height = self.blogCollectionView.frame.height / 3
//            l
//                return CGSize(width:itemWidht, height: height)
//
//        }else{
//
//        }
//
//        return CGSize()
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.categorisCollection {
            currentCatIndex = indexPath.row
            if let catArray = blogpost[indexPath.row]["posts"].array{
            blogCategorisPost = catArray
            
              }
            categorisCollection.reloadData()
            let index = indexPath.row
            switch index {
                  case 0:
                      AnimatableReload.reload(collectionView: self.blogCollectionView, animationDirection: "up")
                  case 1:
                      AnimatableReload.reload(collectionView: self.blogCollectionView, animationDirection: "down")
                  case 2:
                      AnimatableReload.reload(collectionView: self.blogCollectionView, animationDirection: "left")
                  case 3:
                      AnimatableReload.reload(collectionView: self.blogCollectionView, animationDirection: "right")
                  default:
                      break
            }
        }else{
            blogIndex = indexPath.row
           navigateToBlogDetails()
        }

    }

    
    
}
extension BlogController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.SECOND {
                  getBlogResponse(data, statusCode: statusCode)
              }
    }
}
