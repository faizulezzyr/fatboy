//
//  BlogDetailsController.swift
//  FatBoy
//
//  Created by Innovadeaus on 11/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class BlogDetailsController: UIViewController {
    @IBOutlet weak var CoverImage: UIImageView!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showData()
        // Do any additional setup after loading the view.
    }
    

    func showData(){
        if let url = blogCategorisPost[blogIndex]["feature_photo"].string {
            let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            let profilePicURL = URL(string: urlString!)
             CoverImage.kf.indicatorType = .activity
            //cell?.coverImage.kf.setImage(with: profilePicURL, placeholder: "placeholder")
              CoverImage.kf.setImage(with: profilePicURL)
        }

        if let createdDate = blogCategorisPost[blogIndex]["created_at"].string{
            
           let publishDate  = Commons.getStringToDate(createdDate)
            let currentTime = Commons.getLocalDate()
            let differnce = Commons.calculateTime(currentDate: currentTime, createdDate: publishDate)
            time.text = differnce
        }
        if let details = blogCategorisPost[blogIndex]["contents"].string{
            self.textView.attributedText = details.convertHTML()
            self.textView.font = UIFont.systemFont(ofSize: 15)
            self.textView.textAlignment = .justified
        }
        if let titleNa = blogCategorisPost[blogIndex]["title"].string{
            self.titleName.text = titleNa
        }

    }

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
