//
//  Commons.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 18/4/17.
//  Copyright © 2017 Bitmascot. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

open class Commons: NSObject {

    static func makeCardView(view: UIView) -> UIView {
        
        view.layer.masksToBounds = false
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.cornerRadius = 5
        view.layer.shadowRadius = 1.5
        view.layer.shadowOpacity = 0.3
        
        return view
    }
    
    static func makeCircularFloatingView(view: UIView) -> UIView {
        
        view.layer.masksToBounds = false
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.cornerRadius = view.frame.size.width / 2.0
        view.layer.shadowRadius = 1
        view.layer.shadowOpacity = 0.1
        return view
    }

    
    static func colorWithHexString (_ hex:String) -> UIColor {
        
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
                 
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    static func getLeftRightView(_ imageName: String) -> UIView {
                
        let imageView = UIImageView()
        imageView.image = UIImage(named: imageName)
        
        let view = UIView()
        view.addSubview(imageView)
        
        view.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imageView.frame = CGRect(x: 5, y: 5, width: 20, height: 20)
        imageView.contentMode = .scaleAspectFit
        
        return view
    }
    
    static func userExist() -> Bool {

        let userExist = Helpers.getStringValueForKey(Constants.EMAIL)
        if userExist.count > 0 {
            return true
        }
        return false
    }
    
    static func getFormattedDate(_ date: Date, format: String) -> String {
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = format
        let result = formatter.string(from: date)
        
        return result
    }

   static func diffrenceDate(currentDate: Date, picDate: Date ) -> String{
        let difference = Calendar.current.dateComponents([.hour, .minute], from: currentDate, to: picDate)
        let formattedString = String(format: "%02ld%02ld", difference.hour!, difference.minute!)
        return formattedString
    }
    static func calculateTime(currentDate: Date, createdDate: Date ) -> String{
        let difference = Calendar.current.dateComponents([.month, .day, .hour, .minute], from: createdDate, to: currentDate)
        let formattedString = String(format: "%02ld%02ld", difference.day!, difference.hour!, difference.minute!)
        var timeString = ""
        
        if difference.minute != 0 {
            timeString = "\(String(describing: difference.minute!)) minute"
        }
        if difference.hour != 0 {
            timeString = "\(String(describing: difference.hour!)) hours \(String(describing: difference.minute!)) minute"
        }
        if difference.day != 0 {
            timeString = "\(String(describing: difference.day!)) Day  \(String(describing: difference.hour!)) hours"
        }
        if difference.month != 0 {
            timeString = "\(String(describing: difference.month!)) Month \(String(describing: difference.day!)) Day"
        }
        return timeString
    }
    static func diffrenceDay(currentDate: Date, picDate: Date ) -> String{
        let difference = Calendar.current.dateComponents([.day], from: currentDate, to: picDate)
        let formattedString = String(difference.day!)
        return formattedString
    }
    static func getLocalDate() -> Date {
      let timestamp = NSDate().timeIntervalSince1970
      let myTimeInterval = TimeInterval(timestamp)
      let time = NSDate(timeIntervalSince1970: TimeInterval(myTimeInterval))
        
        return time as Date
    }
    
    static func getDate(_ dateString: String) -> Date {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = formatter.date(from: dateString)!
        return date
    }
    
    static func isDateInRange(_ startDate: Date, endDate: Date) -> Bool {
        let currentDate = Date()
        if currentDate >= startDate && currentDate <= endDate {
            return true
        }
        return false
    }
    
    static func changeDateFormat(_ dateString: String, curformat: String, desiredFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = curformat
        let date = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = desiredFormat
        let dateString = dateFormatter.string(from: date!)
        return dateString
    }
    
    static func changeTimeFormat(_ timeString: String, curformat: String, desiredFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = curformat
        let times = dateFormatter.date(from: timeString)
        dateFormatter.dateFormat = desiredFormat
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        let time = dateFormatter.string(from: times!)
        return time
    }
    
    static func getStringToDate(_ dateString: String) -> Date {
        
       let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
       let date = dateFormatter.date(from: dateString)
        return date!
    }
    
    static func getDateInAmPmFormat(_ date: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let dateString = formatter.string(from: date)
        return dateString
    }
    
    static func getEmptyDataMessage(_ delegate: AnyObject?) -> UILabel {
        
        if let view = delegate?.view {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: (view.bounds.size.width), height: (view.bounds.size.height)))
            messageLabel.text = "No referrals found."
            messageLabel.textColor = Commons.colorWithHexString(Colors.DISSELECTED_TEXT_COLOR)
            messageLabel.textAlignment = NSTextAlignment.center
            messageLabel.numberOfLines = 0
            messageLabel.font = UIFont.systemFont(ofSize: 16)
            
            return messageLabel
        }
        return UILabel()
    }
    
    static func getEmptyMessage(_ delegate: AnyObject?, message: String) -> UILabel {
        
        if let view = delegate?.view {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: (view.bounds.size.width), height: (view.bounds.size.height)))
            messageLabel.text = message
            messageLabel.textColor = Commons.colorWithHexString(Colors.DISSELECTED_TEXT_COLOR)
            messageLabel.textAlignment = NSTextAlignment.center
            messageLabel.numberOfLines = 0
            messageLabel.font = UIFont.systemFont(ofSize: 16)
            
            return messageLabel
        }
        return UILabel()
    }
    
    static func getUnderConstructionMessage(_ delegate: AnyObject?) -> UILabel {
        
        if let view = delegate?.view {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: (view.bounds.size.width), height: (view.bounds.size.height)))
            messageLabel.text = "This Feature is Under Construction!!!"
            messageLabel.textColor = Commons.colorWithHexString(Colors.DISSELECTED_TEXT_COLOR)
            messageLabel.textAlignment = NSTextAlignment.center
            messageLabel.numberOfLines = 0
            messageLabel.font = UIFont(name: Constants.FONT_NAME_GOTHAM, size: 16.0)
            
            return messageLabel
        }
        return UILabel()
    }
    
    static func getValue(_ value: String?) -> String {
        
        let result = (value ?? "").isEmpty ? "" : value!
        return result
    }
    
    static func getDelay() -> DispatchTime {
        let when = DispatchTime.now() + 0.5
        return when
    }
    
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    static func resizeImage(_ image: UIImage, targetSize: CGSize) -> UIImage {
        
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    static func configNavBar() {
        
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = colorWithHexString("#1EA2CB")
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().clipsToBounds = false
        UINavigationBar.appearance().backgroundColor = colorWithHexString("#1EA2CB")
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }


    static func removeSavedParams() {
        
    }
    static func wordToRemove(sentence: String, word: String) -> String {
      
        var currentSentence = sentence
        currentSentence = (sentence.components(separatedBy: NSCharacterSet.decimalDigits) as NSArray).componentsJoined(by: "")
        let wordToRemove = word
        
        
        if let range = currentSentence.range(of: wordToRemove) {
            currentSentence.removeSubrange(range)
        }
        let string2 = currentSentence.replacingOccurrences(of: "/", with: "")
        let retrunValue = string2.replacingOccurrences(of: " ", with: "")
        let removeUnnamedRouad = retrunValue.replacingOccurrences(of: "", with: "")
        let retrunValueFinal = retrunValue.replacingOccurrences(of: "'", with: "")
        return retrunValueFinal
    }
    
    static func paddingViewWithImage(_ iconName: String, textField: UITextField) -> UIView {
        
        let iconWidth = 20
        let iconHeight = 20
        
        let imageView = UIImageView()
        let imageEmail = UIImage(named: iconName)
        imageView.image = imageEmail
        
        imageView.frame = CGRect(x: 0, y: 5, width: iconWidth, height: iconHeight)
        textField.leftViewMode = UITextField.ViewMode.always
        textField.addSubview(imageView)
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 25, height: textField.frame.height))
        return paddingView
    }
    
    static func swiftyJSONArrayToString(_ jsonArray: [JSON], key: String) -> String {
        var object = [String: [JSON]]()
        object[key] = jsonArray
        let result = JSON(object)
        if let resultString = result.rawString() {
            return resultString
        }
        return ""
    }
    static func destroyData(){
        Helpers.removeValue(Constants.PROFILE_INFO)
        Helpers.removeValue(Constants.USER_ID)
        Helpers.removeValue(Constants.ACCESS_TOKEN)
        Helpers.removeValue(Constants.MEMBER_ID)
    }
    
    static func stringToSwiftyJSONArray(_ resultString: String, key: String) -> [JSON] {
        let emptyArray: [JSON] = [JSON]()
        let result = JSON.init(parseJSON: resultString)
        if let jsonArray = result[key].array {
            return jsonArray
        }
        return emptyArray
    }
    
    static func getDeviceId() -> String {
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }

    func placeHolderColorChange(TextFild: UITextField, PlaceHolder: String){
        TextFild.attributedPlaceholder = NSAttributedString(string: PlaceHolder, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
 
    }
    
    static func bottomCornerRedious(button: UIButton, cornerRedious: Int){
        button.layer.cornerRadius = CGFloat(cornerRedious)
        button.layer.masksToBounds = true
        
    }
    
    func labelCornerRedious(label: UILabel, cornerRedious: Int){
        label.layer.cornerRadius = CGFloat(cornerRedious)
        label.layer.masksToBounds = true
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    func jsonToNSData(json: AnyObject) -> NSData?{
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }

}


















