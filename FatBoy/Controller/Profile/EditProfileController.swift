//
//  EditProfileController.swift
//  FatBoy
//
//  Created by Innovadeaus on 2/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import SwiftyJSON

class EditProfileController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var emaill: UITextField!
    @IBOutlet weak var dob: UITextField!
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var height: UITextField!
    @IBOutlet weak var goalWeight: UITextField!
    @IBOutlet weak var weightInchButton: UIButton!
    
    @IBOutlet weak var kgButton: UIButton!
    
    @IBOutlet weak var goalwightButton: UIButton!
    
    @IBOutlet weak var period: UITextField!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    
    
    var userService : UserService?
    var apiCommunicatorHelper: APICommunicator?
    let alart = SweetAlert()
    var gender = 0
    let piker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateButton.layer.cornerRadius = 16
        updateButton.clipsToBounds = true
        initApiCommunicatorHelper()
        self.hideKeyboardWhenTappedAround()
        createpiker()
        dob.delegate = self
        showData()
        // Do any additional setup after l oading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    func showData(){
        if let data = profileData["data"].dictionary{
            if let firstN = data["first_name"]?.string{
                self.firstName.text = firstN
            }
            if let lastN = data["last_name"]?.string{
                self.lastName.text = lastN
            }
            if let email = data["email"]?.string{
                self.emaill.text = email
            }
            if let dateOfBirth = data["DOB"]?.string{
                self.dob.text = dateOfBirth
            }
            if let gender = data["gender"]?.int{
                if gender == 0 {
                    self.femaleButton.setImage(UIImage(named: "selected"), for: .normal)
                    self.maleButton.setImage(UIImage(named: "notselected"), for: .normal)
                }else{
                    self.maleButton.setImage(UIImage(named: "selected"), for: .normal)
                    self.femaleButton.setImage(UIImage(named: "notselected"), for: .normal)
                }
            }
            if let weightt = data["weight"]?.string{
                self.weight.text = weightt
            }
            if let heightt = data["height"]?.string{
                self.height.text =  heightt
            }
            if let goalW = data["target_weight"]?.string{
                self.goalWeight.text = goalW
            }
            if let periodd = data["target_period"]?.int{
                self.period.text = String(periodd)
            }
        }
        print(profileData)
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        userService = UserService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    func createpiker(){
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donepress))
        toolbar.setItems([done], animated: false)
        dob.inputAccessoryView = toolbar
        dob.inputView = piker
        piker.datePickerMode = .date
    }
    @objc func donepress(){
        let dateFormater = DateFormatter()
        dateFormater.dateStyle = .short
        dateFormater.timeStyle = .none
        let dateString = dateFormater.string(from: piker.date)
        dob.text = "\(dateString)"
        self.view.endEditing(true)
    }
    
    @IBAction func male(_ sender: Any) {
        maleButton.setImage(UIImage(named: "selected"), for: .normal)
        femaleButton.setImage(UIImage(named: "notselected"), for: .normal)
        gender = 1
        
    }
    
    @IBAction func female(_ sender: Any) {
        maleButton.setImage(UIImage(named: "notselected"), for: .normal)
        femaleButton.setImage(UIImage(named: "selected"), for: .normal)
        gender = 0
    }
    

    
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func update(_ sender: Any) {
        let firstN = firstName.text ?? ""
        let lastN = lastName.text ?? ""
        let email = emaill.text ?? ""
        let doB = dob.text ?? ""
        let weighT = weight.text ?? ""
        let heighT = height.text ?? ""
        let goalW = goalWeight.text ?? ""
        let perioD = period.text  ?? ""
        
        if firstN != "" && lastN != "" && email != "" && doB != "" && weighT != "" && heighT != "" {
            updateProfile(fName: firstN, mName: "", lName: lastN, email: email, dob: doB, height: heighT, weight: weighT, targetWeight: goalW, targetPeriod: perioD, gender: gender)
        }else{
             alart.showAlert("Required", subTitle: "Missing input field", style: .error)
        }
    }
    
    func updateProfile(fName: String, mName: String, lName : String, email : String, dob : String, height : String , weight : String, targetWeight : String, targetPeriod : String, gender : Int ){
        
   
        let param = [
            "first_name" : fName,
            "middle_name" : mName,
            "last_name" : lName,
            "date_of_birth":  dob,
            "email" : email,
            "height" : height,
            "weight" : weight,
            "target_weight" : targetWeight,
            "target_period" : targetPeriod,
            "gender" : String(gender),
            "occupation": "st"

        ]
        print(param)
        userService?.updateProfile(_param: param)
    }
    
    func getProfileUpdateResponse(_ data: [String: Any], statusCode: Int) {
                
                if statusCode == Constants.STATUS_CODE_SUCCESS {
                    let response = JSON(data)
                    print(response)
                    if !response.isEmpty {
                        if let success = response["success"].bool{
                            if success == true{
                                if let msg = response["message"].string{
                                    alart.showAlert("success", subTitle: msg, style: .success)
                                     self.navigationController?.popViewController(animated: true)
                                }
                            }else{
                                if let msg = response["message"].string{
                                    alart.showAlert("Faild", subTitle: msg, style: .error)
                                }
                               
                            }
                        }
                        
                     }
                    
                }else{
                   let response = JSON(data)
                    if let msg = response["message"].string{
                       alart.showAlert("Faild", subTitle: msg, style: .error)
                    }
                }
                
              }
    
}

extension EditProfileController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
                  getProfileUpdateResponse(data, statusCode: statusCode)
              }
    }
}
extension EditProfileController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)

    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

