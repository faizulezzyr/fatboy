//
//  ProfileController.swift
//  FatBoy
//
//  Created by Innovadeaus on 2/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import SwiftyJSON

var profileData : JSON = JSON()
class ProfileController: UIViewController,UIPopoverPresentationControllerDelegate {
    @IBOutlet weak var personalLabel: UILabel!
    @IBOutlet weak var otherLabel: UILabel!
    @IBOutlet weak var personalView: UIView!
    @IBOutlet weak var otherView: UIView!
    @IBOutlet weak var personalButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var EditButton: UIButton!
    @IBOutlet weak var trainerName: UILabel!
    @IBOutlet weak var JoinDate: UILabel!
    @IBOutlet weak var package: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var Email: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var dateofBirth: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var gymAddress: UILabel!
    
    
    var userService : UserService?
    var apiCommunicatorHelper: APICommunicator?
    let alart = SweetAlert()
    let imagePicker = UIImagePickerController()
    var currUploadedPhoto = UIImage()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initApiCommunicatorHelper()
 
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
          getProfileData()
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        userService = UserService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    func initView(){
        personalView.isHidden = false
        otherView.isHidden = true
        EditButton.layer.cornerRadius = 16
        EditButton.clipsToBounds = true
        imagePicker.delegate = self
        ProfileImage.layer.cornerRadius = 70
        ProfileImage.clipsToBounds = true
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(profileTapped(tapGestureRecognizer:)))
           ProfileImage.isUserInteractionEnabled = true
           ProfileImage.addGestureRecognizer(tapImage)
    }
    @objc func profileTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        uploadPictureActionSheet()
     }
    

    @IBAction func personalAction(_ sender: Any) {
        personalView.isHidden = false
        otherView.isHidden = true
        personalLabel.textColor = Commons.colorWithHexString("F7941D")
        otherLabel.textColor = UIColor.white
        personalButton.setImage(UIImage(named: "Parsonalselect"), for: .normal)
        otherButton.setImage(UIImage(named: "Unselect"), for: .normal)
    }
    @IBAction func otherAction(_ sender: Any) {
        personalView.isHidden = true
        otherView.isHidden = false
        otherLabel.textColor = Commons.colorWithHexString("F7941D")
        personalLabel.textColor = UIColor.white
        personalButton.setImage(UIImage(named: "ParsonalUnselect"), for: .normal)
        otherButton.setImage(UIImage(named: "Othersselect"), for: .normal)

    }
    func navigateToEdit(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "edit") as! EditProfileController
        self.navigationController?.show(nextViewController, sender:true)
    }
    func uploadPictureActionSheet() {
          
          let sheet = UIAlertController(title: "Upload Picture", message: nil, preferredStyle: .actionSheet)
          
          let takePhotoAction = UIAlertAction(title: "Take Photo", style: .default) { (action) in
              
              if UIImagePickerController.isSourceTypeAvailable(.camera) {
                  self.imagePicker.allowsEditing = false
                  self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                  self.imagePicker.cameraCaptureMode = .photo
                  self.imagePicker.modalPresentationStyle = .fullScreen
                  self.present(self.imagePicker,animated: true,completion: nil)
              }
              else{
                  let popup = CustomAlertPopup.customOkayPopup("No Camera", msgBody: "This device has no camera!!!")
                  self.present(popup, animated: true, completion: nil)
              }
          }
          
          let photoLibraryAction = UIAlertAction(title: "Choose from Gallery", style: .default) { (action) in
              self.imagePicker.allowsEditing = false
              self.imagePicker.sourceType = .photoLibrary
              self.present(self.imagePicker, animated: true, completion: nil)
          }
          
          let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
          
          sheet.addAction(takePhotoAction)
          sheet.addAction(photoLibraryAction)
          sheet.addAction(cancelAction)
          
          if UIDevice.current.userInterfaceIdiom == .pad {
              sheet.popoverPresentationController?.sourceView = self.view
              sheet.popoverPresentationController?.permittedArrowDirections = []
              sheet.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.height - 140, width: 0, height: 0)
              self.present(sheet, animated: true, completion: nil)
          }
          else {
              self.present(sheet, animated: true, completion: nil)
          }
      }
    
    @IBAction func edit(_ sender: Any) {
        navigateToEdit()
    }
    func doUploadProfilePicture(_ image: UIImage) {
        let data = image.pngData()
        let memberId = Helpers.getIntValueForKey(Constants.MEMBER_ID)
        let params = [
            "member_id": String(memberId)
        ]
        userService?.uploadProfileImage(params, imageData: data!)
    }
    func getProfileData(){
        userService?.getProfile()
    }
    func getProfileResponse(_ data: [String: Any], statusCode: Int) {
          if statusCode == Constants.STATUS_CODE_SUCCESS {
              let response = JSON(data)
              print(response)
              profileData =  response
              if !response.isEmpty {
                if let data = response["data"].dictionary{
                    if let firstName = data["first_name"]?.string{
                        self.userName.text = firstName
                        if let lastN = data["last_name"]?.string{
                            let name = firstName + " " + lastN
                            self.userName.text = name
                            Helpers.setStringValueWithKey(name, key: Constants.FULLNAME)
                        }else{
                            Helpers.setStringValueWithKey(firstName, key: Constants.FULLNAME)
                         }
                       }
                      if let url = data["photo_url"]?.string {
              
                          let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                          let profilePicURL = URL(string: urlString!)
                          ProfileImage.kf.indicatorType = .activity
                          ProfileImage.kf.setImage(with: profilePicURL)
                    }
                    if let email = data["email"]?.string{
                             self.Email.text = email
                         }
                    if let mobile = data["contact"]?.string{
                             self.mobileNumber.text = mobile
                         }
                    if let heightV = data["height"]?.string{
                             self.height.text = heightV
                         }
                    if let weight = data["weight"]?.string{
                        self.weight.text = weight
                    }
                    if let address = data["address"]?.string{
                             self.address.text = address
                         }
                    if let dob = data["DOB"]?.string{
                             self.dateofBirth.text = dob
                         }
                    if let dateofJ = data["created_at"]?.string{
                        let convertDate = Commons.changeDateFormat(dateofJ, curformat: "yyyy-MM-dd HH:mm:ss", desiredFormat: "MMM dd,yyyy")
                             self.JoinDate.text = convertDate
                         }
                    if let package = data["subscriptions"]?.dictionary{
                            if let packageName = package["plan_name"]?.string{
                              self.package.text = packageName
                            }
                        }
                    if let branch = data["branch"]?.dictionary{
                        if let branchName = branch["name"]?.string{
                            self.trainerName.text = branchName
                        }
                        if let branchAddress = branch["address"]?.string{
                            self.gymAddress.text = branchAddress
                        }
                    }
                }
            }
        }
    }
    func getUploadProfilePictureResponse(_ data: [String: Any], statusCode: Int) {
           var success = false
           if statusCode == Constants.STATUS_CODE_SUCCESS{
               let response = JSON(data)
            print(response)
               if !response.isEmpty{
                   if let updateChangeStatus = response["success"].bool {
                       success = updateChangeStatus
                       if success {
                        if let data = response["data"].dictionary{
                            if let url = data["photo"]?.string {
                                Helpers.setStringValueWithKey(url, key: Constants.USER_IMAGE)
                                let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                let profilePicURL = URL(string: urlString!)
                                ProfileImage.kf.indicatorType = .activity
                                ProfileImage.kf.setImage(with: profilePicURL)
                             DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                                              
                            }
                          }
                        }
                          
                       }
                       else{
                           if let reson = response["message"].string {
                               let popup = CustomAlertPopup.customOkayPopup("", msgBody: reson)
                               self.present(popup, animated: true, completion: nil)
                           }
                       }
                   } 
               }
           }
       }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

extension ProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        
        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            let image = Commons.resizeImage(pickedImage, targetSize: CGSize(width: Constants.RESZIE_IMAGE_WIDTH, height: Constants.RESZIE_IMAGE_HEIGHT))
            currUploadedPhoto = image
            doUploadProfilePicture(currUploadedPhoto)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
extension ProfileController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getProfileResponse(data, statusCode: statusCode)
        }else if methodTag == MethodTags.SECOND{
            getUploadProfilePictureResponse(data, statusCode: statusCode)
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
