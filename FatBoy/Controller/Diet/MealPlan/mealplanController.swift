//
//  mealplanController.swift
//  FatBoy
//
//  Created by Innovadeaus on 12/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import AnimatableReload

class mealplanController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
       
       override func viewDidLoad() {
           super.viewDidLoad()

           // Do any additional setup after loading the view.
           AnimatableReload.reload(tableView: tableView, animationDirection: "right")
       }
       @IBAction func back(_ sender: Any) {
             navigationController?.popViewController(animated: true)
         }
       func navigateToMealDetails(){
           let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
           let nextViewController = storyBoard.instantiateViewController(withIdentifier: "mealDetails") as! MealDetailsController
           self.navigationController?.show(nextViewController, sender:true)
       }


}
extension mealplanController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? mealCell
        
         cell?.backView.setCardView()
        cell?.Foodimage.layer.cornerRadius = 35
        cell?.Foodimage.clipsToBounds = true
         return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigateToMealDetails()
    }
    
    

}
