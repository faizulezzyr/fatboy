//
//  mealCell.swift
//  FatBoy
//
//  Created by Innovadeaus on 12/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class mealCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var Foodimage: UIImageView!
    @IBOutlet weak var name: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
