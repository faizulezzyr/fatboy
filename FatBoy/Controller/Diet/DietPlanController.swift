//
//  DietPlanController.swift
//  FatBoy
//
//  Created by Innovadeaus on 5/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import CollectionViewSlantedLayout
class DietPlanController: UIViewController {

     @IBOutlet weak var collectionViewLayout: CollectionViewSlantedLayout!
        
        @IBOutlet weak var collectionView: UICollectionView!
    
        let reuseIdentifier = "customViewCell"


        override func viewDidLoad() {
            super.viewDidLoad()
            navigationController?.isNavigationBarHidden = true
            collectionViewLayout.isFirstCellExcluded = true
            collectionViewLayout.isLastCellExcluded = true
            collectionViewLayout.slantingDirection = .upward
            collectionViewLayout.scrollDirection = .vertical
            collectionViewLayout.zIndexOrder = .ascending
            collectionViewLayout.slantingSize = 0
            collectionViewLayout.lineSpacing = 0
                  let rect = CGRect(x: 0, y: 0, width: 0, height: 0)
            self.collectionViewLayout.collectionView?.scrollRectToVisible(rect, animated: true)
        }

        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

        }

        override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
            return UIStatusBarAnimation.slide
        }
    func navigateToMealPlan(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "mealplan") as! mealplanController
        self.navigationController?.show(nextViewController, sender:true)
    }

}
extension DietPlanController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
                            as? DietPlanCell else {
            fatalError()
        }

      //  cell.image = UIImage(named: covers[indexPath.row]["picture"]!)!

        if let layout = collectionView.collectionViewLayout as? CollectionViewSlantedLayout {
            cell.contentView.transform = CGAffineTransform(rotationAngle: layout.slantingAngle)
        }

        return cell
    }
}

extension DietPlanController: CollectionViewDelegateSlantedLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigateToMealPlan()
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: CollectionViewSlantedLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGFloat {
        return collectionViewLayout.scrollDirection == .vertical ? 275 : 325
    }
}

extension DietPlanController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let collectionView = collectionView else {return}
        guard let visibleCells = collectionView.visibleCells as? [DietPlanCell] else {return}
        for parallaxCell in visibleCells {
            let yOffset = (collectionView.contentOffset.y - parallaxCell.frame.origin.y) / parallaxCell.imageHeight
            let xOffset = (collectionView.contentOffset.x - parallaxCell.frame.origin.x) / parallaxCell.imageWidth
            parallaxCell.offset(CGPoint(x: xOffset * xOffsetSpeed, y: yOffset * yOffsetSpeed))
        }
    }
}
