//
//  InvoiceController.swift
//  FatBoy
//
//  Created by Innovadeaus on 18/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import SwiftyJSON
import AnimatableReload

class InvoiceController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var colorCode = ["FF9500","00A14B","EC008C"]
    var userService : UserService?
    var apiCommunicatorHelper: APICommunicator?
    let alart = SweetAlert()
    var invoiceList : [JSON] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        initApiCommunicatorHelper()
        getInvoice()
        // Do any additional setup after loading the view.
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        userService = UserService(self.view, communicator: apiCommunicatorHelper!)
     
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func back(_ sender: Any) {
        navigateToHome()
    }
    
    func getInvoice(){
        userService?.getInvoice()
    }
    
    func getInvoiceResponse(_ data: [String: Any], statusCode: Int) {
                
                if statusCode == Constants.STATUS_CODE_SUCCESS {
                    let response = JSON(data)
                    print(response)
                    if !response.isEmpty {
                        if let success = response["success"].bool{
                            if success == true{
                              if let data = response["data"].dictionary{
                                if let invoiceData = data["data"]?.array{
                                    self.invoiceList = invoiceData
                                    AnimatableReload.reload(tableView: tableView, animationDirection: "down")
                                }
                              }
                            }else{
                                if let msg = response["message"].string{
                                    alart.showAlert("Faild", subTitle: msg, style: .error)
                                }
                               
                            }
                        }
                        
                     }
                    
                }else{
                   let response = JSON(data)
                    if let msg = response["message"].string{
                       alart.showAlert("Faild", subTitle: msg, style: .error)
                    }
                }
                
              }

}
 
extension InvoiceController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return invoiceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? invoiceCell
        
        if let invoID = invoiceList[indexPath.row]["invoice_number"].string{
           cell?.invoiceId.text = "Invoice id: \(invoID)"
        }
        if let pendingAmmount = invoiceList[indexPath.row]["pending_amount"].int{
            if pendingAmmount != 0 {
                cell?.paymentImage.image = UIImage(named: "paid")
                cell?.colorView.backgroundColor = Commons.colorWithHexString("00A14B")
            }else{
                cell?.paymentImage.image = UIImage(named: "due")
                cell?.colorView.backgroundColor = Commons.colorWithHexString("F26523")
            }
        }
        if let ammount = invoiceList[indexPath.row]["total"].int {
            cell?.ammount.text = "BDT \(ammount)/-"
        }
        if let date = invoiceList[indexPath.row]["invoice_date"].string{
            
            let convertDate = Commons.changeDateFormat(date, curformat: "yyyy-MM-dd HH:mm:ss", desiredFormat: "MMM dd,yyyy")
            cell?.date.text = "Date : \(convertDate) "
        }
        cell?.number.text = String(indexPath.row + 1)
        cell?.backView.setCardViewForInvoice()
        return cell!
    }
    
    
}
extension InvoiceController : APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
                  getInvoiceResponse(data, statusCode: statusCode)
              }
    }
}
