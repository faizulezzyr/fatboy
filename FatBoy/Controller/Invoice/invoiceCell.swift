//
//  invoiceCell.swift
//  FatBoy
//
//  Created by Innovadeaus on 18/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit

class invoiceCell: UITableViewCell {
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var invoiceId: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var ammount: UILabel!
    @IBOutlet weak var paymentImage: UIImageView!
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var colorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
 
