//
//  Urls.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 25/4/17.
//  Copyright © 2017 WebAlive. All rights reserved.
//

import Foundation

class Urls: NSObject {
 
    static var BASE_URL = "http://api-fatboygym.ipllab.net/v1/"
    static var LOGIN_URL = "login"
    static var BLOG_POST = "blog-posts"
    static var ATTENDNCE_LIST = "attendance"
    static var ATTENDANCE = "attendance/store"
    static var PRICE_PLAN = "subscription-packages"
    static var PROFILE = "profile"
    static var INVOICE = "invoice"
    static var NOTIFICATION = "notifications"
    static var UPDATE_PROFILE = "profile/update"
    static var UPLOAD_IMAGE = "profile/upload-image"
    static var SETTING = "settings"
    static var CONTACT_LIST = "branch"

    
    // GOOGLE MAP API
    static var GOOGLE_MAP_GEOGOCE_BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json?"
    static var GOOGLE_MAP_DIRECTION_API_BASE_URL = "https://maps.googleapis.com/maps/api/directions/json?"
    static var GOOGLE_PLACES_SEARCH_API_BASE_URL = "https://maps.googleapis.com/maps/api/place/textsearch/json?"
    static var GOOGLE_MAP_DISTANCE_MATRIX_API_BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?"
    
    // URL ENCODED KEYS

}





