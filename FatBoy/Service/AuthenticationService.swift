//
//  AuthenticationService.swift
//
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class AuthenticationService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    

    func doLogin(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.LOGIN_URL
        print(url)
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: false)
    }
    

}




