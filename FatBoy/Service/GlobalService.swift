//
//  GlobalService.swift
//  FatBoy
//
//  Created by Innovadeaus on 15/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//


import UIKit
import Foundation
import Alamofire

class GlobalService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    func getBlogs(){
        let url = Urls.BASE_URL + Urls.BLOG_POST
        print(url)
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.SECOND, withHeader: true)
    }
    func getNotification(){
        let url = Urls.BASE_URL + Urls.NOTIFICATION
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: true)
        
    }
    

    

}
