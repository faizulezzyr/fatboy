//
//  UserService.swift
//  FatBoy
//
//  Created by Innovadeaus on 15/2/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//


import UIKit
import Foundation
import Alamofire

class UserService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    func Attendance(_param : Parameters){
        let url = Urls.BASE_URL + Urls.ATTENDANCE
        apiCommunicatorHelper?.getDataByPOST(url, params: _param, methodTag: MethodTags.FIRST, withHeader: true)
    }
    func getAttendance(){
        let url = Urls.BASE_URL + Urls.ATTENDNCE_LIST
        print(url)
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: true)
    }
    func getPricePlan(){
        let url = Urls.BASE_URL + Urls.PRICE_PLAN
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: true)
    }
    func getProfile(){
        let url = Urls.BASE_URL + Urls.PROFILE
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: true)
    }
    
    func getInvoice(){
        let url = Urls.BASE_URL + Urls.INVOICE
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: true)
        
    }
    func updateProfile(_param : Parameters){
        let url = Urls.BASE_URL + Urls.UPDATE_PROFILE
        print(url)
        apiCommunicatorHelper?.UpdateDataByPOST(url, params: _param, methodTag: MethodTags.FIRST, withHeader: true)
    }
    func uploadProfileImage(_ params: Parameters, imageData: Data) {
        let url = Urls.BASE_URL + Urls.UPLOAD_IMAGE
        print(url)
        apiCommunicatorHelper?.uploadPhoto(url, imageData: imageData, params: params, methodTag: MethodTags.SECOND, withHeader: true)
    }
    

    

}
