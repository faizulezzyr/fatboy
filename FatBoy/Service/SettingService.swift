//
//  SettingService.swift
//  FatBoy
//
//  Created by Innovadeaus on 1/3/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class SettingService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    

    func getSetting(){
        let url = Urls.BASE_URL + Urls.SETTING
        print(url)
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.THIRD, withHeader: true)
    }
    func getContactList(){
        let url = Urls.BASE_URL + Urls.CONTACT_LIST
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: true)
    }

    

}
