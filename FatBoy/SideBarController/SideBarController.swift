//
//  SideBarController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 11/11/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit


var isComeFromMenu = false
var isOpen = false
class SideBarController: UIViewController {
   // var homeDelegate = HomeController()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var closeButt: UIButton!
    @IBOutlet weak var name: UILabel!
    
    let placeholder = UIImage(named: "logo-text")
    let star = "☆"
    var myTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    func initSetup(){
        profileImage.layer.cornerRadius = 50
        profileImage.clipsToBounds = true
        let nameText = Helpers.getStringValueForKey(Constants.FULLNAME)
        
        self.name.text = nameText
        let profileUrl = Helpers.getStringValueForKey(Constants.USER_IMAGE)
        let urlString = profileUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let profilePicURL = URL(string: urlString!)
        profileImage.kf.indicatorType = .activity
        profileImage.kf.setImage(with: profilePicURL)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    @objc func profileTapped(tapGestureRecognizer: UITapGestureRecognizer) {
      
        
        print("clicked")
    }
    
    @objc func userNameTapped(tapGestureRecognizer: UITapGestureRecognizer) {
       
    }

    @IBAction func closeButton(_ sender: Any) {
        (parent as? BaseController)?.showAndHide()
        
    }
}


extension SideBarController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.menuItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? menuCell
        cell?.name.text = Constants.menuItem[indexPath.row]
        cell?.Icon.image = UIImage(named: "\(Constants.menuItemImage[indexPath.row]).png")

        cell?.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.8, animations: {
            cell?.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
               },completion: { finished in
                UIView.animate(withDuration: 0.4, animations: {
                    cell?.layer.transform = CATransform3DMakeScale(1,1,1)
                   })
           })
        
        return cell!
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return (tableView.bounds.height / 7) - 3
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row

      if index == 0{
      
        navigateToPakage()
         isOpen = false
        }else if index == 1{
        navigateToAttendance() 
         isOpen = false
        }else if index == 2{
        navigateToinvoice()
         isOpen = false
        }else if index == 3{
     
         isOpen = false
        }else if index == 4{
         navigateToblog()
         isOpen = false
        }else if index == 5 {
         navigateToContactUs() 
         isOpen = false
      }else {
         Commons.destroyData()
         initToLogin()
         isOpen = false
      }
        
        
    }

    
    func navigateToinvoice() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let subscription = mainStoryboard.instantiateViewController(withIdentifier: "Invoice") as? InvoiceController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [subscription!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }

    func navigateToPakage() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let subscription = mainStoryboard.instantiateViewController(withIdentifier: "package") as? SubcriptionController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [subscription!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    
    func navigateToContactUs() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let subscription = mainStoryboard.instantiateViewController(withIdentifier: "contact") as? ContactUsController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [subscription!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    func navigateToblog() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let subscription = mainStoryboard.instantiateViewController(withIdentifier: "blogC") as? BlogController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [subscription!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    func navigateToAttendance() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let subscription = mainStoryboard.instantiateViewController(withIdentifier: "attendance") as? AttendanceController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [subscription!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    
    func initToLogin(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let homeController = mainStoryboard.instantiateViewController(withIdentifier: "login") as? LoginController
         let navigationController = UINavigationController()
         navigationController.viewControllers = [homeController!]
         UIApplication.shared.keyWindow?.rootViewController = navigationController
    }


}





